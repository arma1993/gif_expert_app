


export const getGifs = async (category) => { // transformamos esta function en una funcion async await
    const url = `https://api.giphy.com/v1/gifs/search?limit=10&q=${ encodeURI(category) }&api_key=qfqz9gppbdlqO5cw6Ug4YViptw2cmvhq` // creamos la api dentro de la pagina y lo terminamos de estructurar dentro de postman
    
    const resp = await fetch(url);   // creamos una constante con respuesta await buscar ( fecht )la url 
    const { data } = await resp.json(); // La respuesta lo recibimos en formato Json,  usamos desestructuracion de la data pero al tener muchos arreglos 
    // lo decestructuramos como vemos en el codigo de abajo 
    const gifs = data.map( img => {  
      return { // creamos la decestructuracion como vemos 
        id: img.id,
        title: img.title,
        url: img.images?.downsized_medium.url,
      };
    });
    return gifs; // el async regresa una promesa que resuelve la coleccion de mis imagenes 
  };