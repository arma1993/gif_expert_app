


export const GifGridItem = ({ title, url}) => {
    
  // console.log(id, title, url);


return (
    <div className="card animate__animated animate__bounce animate__fadeIn">
        <img src={url} alt={title}/>
        
        <p className="animate__bounceOut">{title}</p>
      
  
  </div>
  )
}
