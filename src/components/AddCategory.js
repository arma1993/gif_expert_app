import React from 'react'
import { useState } from 'react'
import PropTypes from 'prop-types';


export const AddCategory = ({setCategories}) => {
  
    
    const [inputValue, setinputValue] = useState('') //El inputValue tiene por defecto el string '' pero puede ser un arreglo, objeto lo que necesitemos 
    
    const handleInputChange = (e) => { // recibo el el /(e) elemento
        setinputValue (e.target.value); // cambiamos lo que viene predefinido dentro del inputValue ejemplo useState("Hola Mundo");

    
    }
    const andleSubmit = (e) => {  // recibo el (e) evento
       e.preventDefault();     // hace que evite el refresco de la pagina en recargar de nuevo la pagina 
        
      
       if(inputValue.trim().length > 2){
           setCategories(cats => [inputValue, ...cats]);
            setinputValue('');

       }
        
     }

    return (
        // si tenemos un form dentro no hace falta poner un fragment <></> en el caso que tengamos un elemento que reagrupe todos los demas
    <form onSubmit={andleSubmit}> 
        <input 
        type="text"
        value={inputValue}
        onChange={handleInputChange}
        />
    </form>
     )
}
AddCategory.propTypes = {
    setCategories: PropTypes.func.isRequired
  };