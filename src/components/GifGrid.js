import React from "react";
import { useFetchGifs } from "../hooks/useFetchGifs";
// import { useState, useEffect } from "react";
import { GifGridItem } from "./GifGridItem";
// import { getGifs } from "./helpers/GetGifs";

export const GifGrid = ({ category }) => {
  
  // const [images, setImages] = useState([]);

  const {data:images,loading} = useFetchGifs(category);
  // usamos una desestructuracion dentro de data para cambiar el nombre en vez de data usamos images como lo hacemos pues como lo muestra 
//en vez de data.map pues pondriamos images como muestra en el codigo 
 

  // //---------------------------------------------------------------------------------------------------------  
  // useEffect(() => {
  //   getGifs(category)
  //   .then(setImages);    este codigo pasamos a implementarlo en el custom hook 
  //------------------Lo usamos para no crear un ciclo infinito es decir cada vez que enviamos una peticion no nos vuelva a cargar todo el codigo entero 
  // },[category]); // mandamos una lista de dependencias 
 //-----------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------------------------------
  return (
    <>
      <h3 className="animate__animated animate__bounce animate__fadeIn">{category}</h3>
      {loading && <p className="animate__bounceOut">loading</p> }
      
      <div className="card-grid animate__animated animate__bounce animate__fadeIn">
        {images.map((img) => (
          <GifGridItem key={img.id} {...img} />
        ))}
      </div> 
    </>
  );
};
