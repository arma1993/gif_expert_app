import { useState } from "react";
import { AddCategory } from "./components/AddCategory";
import { GifGrid } from "./components/GifGrid";




function GifExpertApp() {
  
  const [categories, setCategories] = useState(['One Punch',])

  //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // const andleAdd = () =>{
  // setcategories([ 'One Piece', ...categories])   ... usando el operador express ----------de esa manera mantenemos las categorias anteriores que hay en el array y agregamos una 
  // setcategories(cats => [ ...cats, 'One Piece']) ----------------cats de categorias con un arrow function luego llamamos a la function con operador express para mantener 
  // nuestras categorias, luego agregamos la que queramos agregar 
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
return(
    <>
      <h2 className="animate__bounceOut">GifExpertApp</h2>
      <AddCategory setCategories={setCategories} />
      <hr/>

      <ol>
        {
          categories.map(category =>(
                <GifGrid 
                  key={category}
                  category={category}
                />
            
            ))
              
        }
      </ol>
      
      
      </>



)
}
export default GifExpertApp;
